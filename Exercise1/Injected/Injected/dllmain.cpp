// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"

#define NEW_TITLE "This NOTEPAD is PWNED by Oren and Yaakov!"

struct handle_data {
    DWORD process_id;
    HWND window_handle;
};

BOOL is_main_window(HWND handle)
{
    return GetWindow(handle, GW_OWNER) == (HWND)0 && IsWindowVisible(handle);
}

BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam)
{
    handle_data* data = (handle_data*)lParam;
    DWORD process_id = 0;
    GetWindowThreadProcessId(handle, &process_id);
    if (data->process_id != process_id || !is_main_window(handle))
        return TRUE;
    data->window_handle = handle;
    return FALSE;
}

HWND find_main_window(DWORD process_id)
{
    handle_data data;
    data.process_id = process_id;
    data.window_handle = 0;
    EnumWindows(enum_windows_callback, (LPARAM)&data);
    return data.window_handle;
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {
        MessageBox(NULL, "Hello from injected dll", "Hello", MB_OK);
        DWORD pid = GetCurrentProcessId();
        HWND window_handle = find_main_window(pid);
        if (window_handle == NULL)
            break;
        SetWindowText(window_handle, NEW_TITLE);
        MessageBox(NULL, "New Notepad Title", "Notepad", MB_OK);
        //FreeLibrary(hModule);
        return FALSE; // we can't call to FreeLibrary because to dll not load yet but return false will free the dll
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

