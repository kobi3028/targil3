import "pe"

rule WindowTitleChangeDLL_YARA {
   meta:
      description 	= 	"This YARA rule aims to detect the injector EXE from exercise 1"
      author 		= 	"Oren and Yaakov"

   strings:
	  
	  /* Unique application strings */
	  $s1 = "D:\\Users\\yaakovco\\Injected\\x64\\Debug\\Injected.dll" fullword ascii
	  $s2 = "Usage: Injector.exe <PID>" fullword ascii
	  $s3 = "Inject PID: " fullword ascii
	  $s4 = "Last Error: " fullword ascii
	  $s5 = "full_dll_path" fullword ascii
	  $s6 = "exit_code" fullword ascii
	  $s7 = "Injected.dll" fullword ascii
   
	  /* DLL injection API strings */
      $s8 = "OpenProcess" fullword ascii
	  $s9 = "VirtualAllocEx" fullword ascii
	  $s10 = "WriteProcessMemory" fullword ascii
	  $s11 = "GetProcAddress" fullword ascii
	  $s12 = "GetModuleHandleA" fullword ascii
	  $s13 = "CreateRemoteThread" fullword ascii
	  
	  /* Visual studio strings */
	  $s14 = "VCRUNTIME140D.dll" fullword ascii 											// runtime library for C++ Visual studio apps
	  
	  /* PDB file location */
	  $s15 = "D:\\Users\\yaakovco\\Injector\\Debug\\Injector.pdb" fullword ascii
	  
   condition:
      uint16(0) == 0x5a4d and filesize < 55KB and pe.number_of_sections == 8 and pe.dll_characteristics & DYNAMIC_BASE and
	  $s8 and $s9 and $s10 and $s11 and $s12 and $s13 and
	  any of ($s1,$s2,$s3,$s4,$s5,$s6,$s7) and $s14
	  or $s15
}