import "pe"

rule WindowTitleChangeDLL_YARA {
   meta:
      description 	= 	"This YARA rule aims to detect the injected DLL from exercise 1"
      author 		= 	"Oren and Yaakov"

   strings:
	  
	  /* Unique application strings */
	  $s1 = "Hello from injected dll" fullword ascii
	  $s2 = "This NOTEPAD is PWNED by Oren and Yaakov!" fullword ascii
	  $s3 = "New Notepad Title" fullword ascii
   
   
	  /* Window title change API strings */
      $s4 = "GetWindow" fullword ascii
	  $s5 = "IsWindowVisible" fullword ascii
	  $s6 = "GetWindowThreadProcessId" fullword ascii
	  $s7 = "EnumWindows" fullword ascii
	  $s8 = "GetCurrentProcessId" fullword ascii
	  $s9 = "SetWindowTextA" fullword ascii
	  
	  /* Visual studio strings */
	  $s10 = "@.msvcjmcc" fullword ascii													// compiled with Visual studio's MSVC
	  $s11 = "VCRUNTIME140D.dll" fullword ascii 											// runtime library for C++ Visual studio apps
	  
	  /* PDB file location */
	  $s12 = "D:\\Users\\yaakovco\\Injected\\Debug\\Injected.pdb" fullword ascii
	  
   condition:
      uint16(0) == 0x5a4d and filesize < 40KB and pe.number_of_sections == 8 and pe.is_dll() and pe.dll_characteristics & DYNAMIC_BASE and
	  $s4 and $s5 and $s6 and $s7 and $s8 and $s9 and
	  any of ($s1,$s2,$s3) and any of ($s10,$s11)
	  or $s12
}

