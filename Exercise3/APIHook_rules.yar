import "pe"

rule APIHooking_YARA {
   meta:
      description 	= 	"This YARA rule aims to detect the API Hooking DLL from exercise 2"
      author 		= 	"Oren and Yaakov"

   strings:
      
	  /* Unique application strings */
	  $s1 = "Hello From Injected DLL" fullword ascii
      $s2 = "Bye-bye From Injected DLL" fullword ascii
	  $s3 = "Func name: " fullword ascii
	  $s4 = "notepad++.exe" fullword wide
	  $s5 = "dont_care" fullword ascii
	  $s6 = "DLL name: " fullword ascii
	  $s7 = "optional_header" fullword ascii
	  $s8 = "data_directory" fullword ascii
	  $s9 = "current_protect" fullword ascii
	  
	  /* API hooking strings */
	  $s10 = "_stricmp" fullword ascii
	  $s11 = "_strnicmp" fullword ascii
	  $s12 = "ntdll.dll" fullword ascii
	  $s13 = "NtQuerySystemInformation" fullword ascii
	  $s14 = "VirtualProtect" fullword ascii
	  
	  /* Visual studio strings */
	  $s15 = "VCRUNTIME140_1D.dll" fullword ascii 											// runtime library for C++ Visual studio apps
      $s16 = "__CxxFrameHandler4" fullword ascii 											// found in x64 Visual studio binaries
      $s17 = "@.msvcjmcc" fullword ascii													// compiled with Visual studio's MSVC
	  $s18 = ".textbss" fullword ascii														// exists if incremental linking is enabled
	  
      /* PDB file location */
      $s19 = "D:\\Users\\yaakovco\\APIHooking\\x64\\Debug\\APIHooking.pdb" fullword ascii	// special file created in debug-compiled programs
	  

   condition:
      uint16(0) == 0x5a4d and filesize < 70KB and pe.number_of_sections == 9 and pe.is_dll() and
	  ($s10 or $s11) and $s12 and $s13 and $s14 or
	  (any of ($s1,$s2,$s3,$s4,$s5,$s6,$s7,$s8,$s9) and any of ($s15,$s16,$s17,$s18))
	  or $s19
}

