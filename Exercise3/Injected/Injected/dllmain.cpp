// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"


struct handle_data {
    DWORD process_id;
    HWND window_handle;
};

typedef HWND  (WINAPI* PGET_WINDOW)(HWND hWnd, UINT uCmd);
typedef BOOL  (WINAPI* PIS_WINDOW_VISIBLE)(HWND hWnd);
typedef DWORD (WINAPI* PGET_WINDOW_THREAD_PROCESS_ID)(HWND hWnd, LPDWORD lpdwProcessId);
typedef BOOL  (WINAPI* PENUM_WINDOWS)(WNDENUMPROC lpEnumFunc, LPARAM lParam);
typedef BOOL  (WINAPI* PSET_WINDOW_TEXT)(HWND hWnd, LPCSTR lpString);
typedef DWORD (WINAPI* PGET_CURRENT_PROCESS_ID)(VOID);

PGET_WINDOW pGetWindow;
PIS_WINDOW_VISIBLE pIsWindowVisible;
PGET_WINDOW_THREAD_PROCESS_ID pGetWindowThreadProcessId;
PENUM_WINDOWS pEnumWindows;
PSET_WINDOW_TEXT pSetWindowText;
PGET_CURRENT_PROCESS_ID pGetCurrentProcessId;

void xorstr(char* enc, DWORD enc_len, char* key, DWORD key_len)
{
    for (size_t i = 0; i < enc_len; i++)
    {
        enc[i] = enc[i] ^ key[i % key_len];
    }
}

BOOL is_main_window(HWND handle)
{
    return pGetWindow(handle, GW_OWNER) == (HWND)0 && pIsWindowVisible(handle);
}

BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam)
{
    handle_data* data = (handle_data*)lParam;
    DWORD process_id = 0;
    pGetWindowThreadProcessId(handle, &process_id);
    if (data->process_id != process_id || !is_main_window(handle))
        return TRUE;
    data->window_handle = handle;
    return FALSE;
}

HWND find_main_window(DWORD process_id)
{
    handle_data data;
    data.process_id = process_id;
    data.window_handle = 0;
    pEnumWindows(enum_windows_callback, (LPARAM)&data);
    return data.window_handle;
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {
        char key[] = "\xC0\xDE";
        
        /*
        user32.dll
        GetWindow; 0x1C3
        IsWindowVisible; 0x223
        GetWindowThreadProcessId; 0x1DD
        EnumWindows; 0x105
        SetWindowText; 0x324
        */

        DWORD ordinal_base = 0x5DE;
        char str5[] = "\xb5\xad\xa5\xac\xf3\xec\xee\xba\xac\xb2";
        xorstr(str5, sizeof(str5) - 1, key, sizeof(key) - 1);
        HMODULE hdll = GetModuleHandle(str5);
        if (hdll == NULL)
            return FALSE;//we can do here also load library but we inject dll to notepad we can assum user32.dll already load
        pGetWindow = (PGET_WINDOW)GetProcAddress(hdll, MAKEINTRESOURCE(0x1C3 + ordinal_base));
        pIsWindowVisible = (PIS_WINDOW_VISIBLE)GetProcAddress(hdll, MAKEINTRESOURCE(0x223 + ordinal_base));
        pGetWindowThreadProcessId = (PGET_WINDOW_THREAD_PROCESS_ID)GetProcAddress(hdll, MAKEINTRESOURCE(0x1DD + ordinal_base));
        pEnumWindows = (PENUM_WINDOWS)GetProcAddress(hdll, MAKEINTRESOURCE(0x105 + ordinal_base));
        pSetWindowText = (PSET_WINDOW_TEXT)GetProcAddress(hdll, MAKEINTRESOURCE(0x324 + ordinal_base));

        /*
        kernel32.dll
        GetCurrentProcessId; 0x210
        */
        ordinal_base = 0x01;
        char str6[] = "\x8b\xbb\xb2\xb0\xa5\xb2\xf3\xec\xee\xba\xac\xb2";
        xorstr(str6, sizeof(str6) - 1, key, sizeof(key) - 1);
        hdll = GetModuleHandle(str6);
        if (hdll == NULL)
            return FALSE;
        pGetCurrentProcessId = (PGET_CURRENT_PROCESS_ID)GetProcAddress(hdll, MAKEINTRESOURCE(0x210 + ordinal_base));

        char str0[] = "\x88\xbb\xac\xb2\xaf\xfe\xa6\xac\xaf\xb3\xe0\xb7\xae\xb4\xa5\xbd\xb4\xbb\xa4\xfe\xa4\xb2\xac";
        xorstr(str0, sizeof(str0) - 1, key, sizeof(key) - 1);
        char str1[] = "\x88\xbb\xac\xb2\xaf";
        xorstr(str1, sizeof(str1) - 1, key, sizeof(key) - 1);
        MessageBox(NULL, str0, str1, MB_OK);
        DWORD pid = pGetCurrentProcessId();
        HWND window_handle = find_main_window(pid);
        if (window_handle == NULL)
            break;
        
        //set window text
        char str2[] = "\x94\xb6\xa9\xad\xe0\x90\x8f\x8a\x85\x8e\x81\x9a\xe0\xb7\xb3\xfe\x90\x89\x8e\x9b\x84\xfe\xa2\xa7\xe0\x91\xb2\xbb\xae\xfe\xa1\xb0\xa4\xfe\x99\xbf\xa1\xb5\xaf\xa8\xe1";
        xorstr(str2, sizeof(str2) - 1, key, sizeof(key) - 1);
        pSetWindowText(window_handle, str2);

        char str3[] = "\x8e\xbb\xb7\xfe\x8e\xb1\xb4\xbb\xb0\xbf\xa4\xfe\x94\xb7\xb4\xb2\xa5";
        xorstr(str3, sizeof(str3) - 1, key, sizeof(key) - 1);
        char str4[] = "\x8e\xb1\xb4\xbb\xb0\xbf\xa4";
        xorstr(str4, sizeof(str4) - 1, key, sizeof(key) - 1);
        MessageBox(NULL, str3, str4, MB_OK);
        //FreeLibrary(hModule);
        return FALSE; // we can't call to FreeLibrary because the dll not load yet but return false will free the dll
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

