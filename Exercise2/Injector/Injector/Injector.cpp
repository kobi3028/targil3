// Injector.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Windows.h>

#define INJECTED_DLL_PATH L"D:\\Users\\yaakovco\\APIHooking\\x64\\Debug\\APIHooking.dll"

using namespace std;
int main(int argc, char** argv)
{
    DWORD exit_code = -1;
    do {
        if (argc < 2)
        {
            cout << "Usage: Injector.exe <PID>" << endl;
            break;
        }

        DWORD pid = atol(argv[1]);
        if (pid == 0)
            break;
        cout << "Inject PID: " << pid << endl;
        
        HANDLE h_process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);         //get handle of injected process
        if (h_process == NULL)
            break;
        
        wchar_t full_dll_path[_MAX_PATH] = INJECTED_DLL_PATH;                      //get the full path of the dll file
        //DWORD str_size = GetFullPathName(INJECTED_DLL_PATH, _MAX_PATH, full_dll_path, NULL);
        //if (str_size > _MAX_PATH)
        //    break;

        LPVOID remote_str_addr = VirtualAllocEx(h_process, NULL, _MAX_PATH*sizeof(wchar_t),
            MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);                         //allocating memory in the injeted process
        if (remote_str_addr == NULL)
            break;
                                                                               //write dll path to the allocate memory
        BOOL res = WriteProcessMemory(h_process, remote_str_addr, full_dll_path, wcslen(full_dll_path)*sizeof(wchar_t), NULL);
        if (res == FALSE)
            break;

        LPVOID func_addr = GetProcAddress(GetModuleHandle("Kernel32"),         //get LoadLibrary address (same across all processes)
            "LoadLibraryW");                             
        if (func_addr == NULL)
            break;

        HANDLE h_rThread = CreateRemoteThread(h_process, NULL, 0,              //start thread at the remote process passing the start function           
            (LPTHREAD_START_ROUTINE)func_addr, remote_str_addr, 0, NULL);      //LoadLibraryW and the dll path str ptr as argument 
        if (h_rThread == NULL)
            break;


        WaitForSingleObject(h_rThread, INFINITE);                              //

        GetExitCodeThread(h_rThread, &exit_code);                              //retrieving the return value of the remote thread

        //getchar();

        CloseHandle(h_rThread);                                                //freeing the injected thread handle

        VirtualFreeEx(h_process, remote_str_addr, 0, MEM_RELEASE);             //freeing the memory allocated for the DLL path str
        
        CloseHandle(h_process);                                                //freeing the remote proces handle

    } while (0);
    cout << "Last Error: " << GetLastError() << endl;
    return exit_code;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
